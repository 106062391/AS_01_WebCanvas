var canvas = document.getElementById('drawboard');
var ctx = canvas.getContext('2d');
var defaultcolor = true;
var colors = ['black', 'pink', 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'purple'];
var fontrange = document.getElementById("fontrange");
var size = document.getElementById("size");
size.innerHTML = fontrange.value;
var draw;
var addingtext = false;
var addingrectangle = false;
var addingtriangle = false;
var addingcircle = false;
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

function mouseposition(canvas, pen){  //simply drawing
    var rect = canvas.getBoundingClientRect();
    return{
         x: pen.clientX - rect.left,
         y: pen.clientY - rect.top
    };
}; 

function mousedwn(pen){
    if(addingtext == false && addingrectangle == false && addingtriangle == false && addingcircle == false)draw = true;
    ctx.beginPath();
}

function mousemov(pen){
    if(draw == true) {
    var mousePos = mouseposition(canvas, pen);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    }
}

function mouseup(){
        draw = false;
        ctx.closePath();
        var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(state, null);
}

window.addEventListener('load', function(){
    canvas.addEventListener('mousedown', mousedwn);
    canvas.addEventListener('mousemove', mousemov);
    canvas.addEventListener('mouseup', mouseup);
})

document.getElementById('clear').addEventListener('click', function() { //reseting
    ctx.clearRect(0, 0, canvas.width, canvas.height);
})

function Changecolorbycolor() { //chaging colors
    ctx.strokeStyle = document.getElementById("curColor").value;
    ctx.fillStyle = document.getElementById("curColor").value;
}

function ChangeColor(i) {
    document.getElementById(colors[i]).addEventListener('click', function(){
    ctx.strokeStyle = colors[i];
    ctx.fillStyle = colors[i];
    document.getElementById("curColor").value = colors[i];
    }, false);
}

function Eraser() {
    ctx.strokeStyle = "white";
    document.getElementById("drawboard").className = "EraserMode";
    addingtext = false;
    addingcircle = false;
    addingrectangle = false;
    addingtriangle = false;
    ctx.strokeStyle = white;
    ctx.fillStyle = white;
    //canvas.getElementById('drawboard').style.cursor = "not-allowed";
}

function Pencil() {
    addingtext = false;
    addingcircle = false;
    addingrectangle = false;
    addingtriangle = false;
    document.getElementById("drawboard").className = "DrawMode";
}

for(var i=0; i<colors.length; i++){
    ChangeColor(i);
}

fontrange.oninput = function() { //changing fonts
    size.innerHTML = fontrange.value;
}

function FontSize() {
        ctx.lineWidth = fontrange.value;
}

canvas.addEventListener('click', function(pen) { //addding text
    if(addingtext == true){
        var mousePos = mouseposition(canvas, pen);
        var text = document.getElementById("text").value;
        var style = document.getElementById("TextStyleSelect").value;
        var style2 = document.getElementById("TextStyleSelect2").value;
        var bold = document.getElementById("bold").checked;


        if(bold == true){
            var fontStyle = 'bold ' + style + ' '+fontrange.value + 'px ' + style2;
        }
        else var fontStyle = style + ' '+fontrange.value + 'px ' + style2;
        console.log(fontStyle);
        ctx.font = fontStyle;
        ctx.fillText(text, mousePos.x, mousePos.y);
    }
})

function Addingtext() {
    document.getElementById("drawboard").className = "TextMode";
    addingtext = true;
    addingcircle = false;
    addingrectangle = false;
    addingtriangle = false;
}

function download() { //downloading images
    var save = document.getElementById("save");
    var image = document.getElementById("drawboard").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    save.setAttribute("href", image);
}

document.getElementById('upload').onchange = function() { //uploading images
    var img = new Image();
    img.onload = draw;
    img.src = URL.createObjectURL(this.files[0]);

    function draw() {
        ctx.drawImage(this, 0,0);
      }
};

window.addEventListener('popstate', changeStep, false); //undo redo

function changeStep(e){
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if( e.state ){
    ctx.putImageData(e.state, 0, 0);
  }
}

function undo() {
    window.history.go(-1);
}

function redo() {
    window.history.go(1);
}

function Addingrectangle() { //adding shapes
    addingrectangle = true;
    addingtext = false;
    addingcircle = false;
    addingtriangle = false;
    document.getElementById("drawboard").className = "RectangleMode";
}

function Addingtriangle() {
    addingtriangle = true;
    addingtext = false;
    addingcircle = false;
    addingrectangle = false;
    document.getElementById("drawboard").className = "TriangleMode";
}

function AddingCircle() {
    addingcircle = true;
    addingtext = false;
    addingrectangle = false;
    addingtriangle = false;
    document.getElementById("drawboard").className = "CircleMode";
}

canvas.addEventListener('click', function(pen) {
    if(addingrectangle == true){
        var range = fontrange.value * 5;
        ctx.beginPath();
        console.log(pen.clientX+' '+pen.clientY);
        ctx.moveTo(pen.clientX, pen.clientY-100);
        ctx.lineTo(pen.clientX+range/2, pen.clientY-100);
        ctx.lineTo(pen.clientX+range/2, pen.clientY-range-100);
        ctx.lineTo(pen.clientX-range*1.5, pen.clientY-range-100);
        ctx.lineTo(pen.clientX-range*1.5, pen.clientY-100);
        ctx.lineTo(pen.clientX-range, pen.clientY-100);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
    else if(addingtriangle == true){
        var range = fontrange.value * 5;
        ctx.beginPath();
        ctx.moveTo(pen.clientX, pen.clientY-100-range);
        ctx.lineTo(pen.clientX+range, pen.clientY-100);
        ctx.lineTo(pen.clientX-range, pen.clientY-100);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
    else if(addingcircle == true){
        var range = fontrange.value * 5;
        ctx.beginPath();
        ctx.moveTo(pen.clientX, pen.clientY-100);
        ctx.arc(pen.clientX, pen.clientY-100, range, 0, 2*Math.PI, true);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }
})