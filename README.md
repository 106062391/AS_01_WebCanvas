﻿# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here


--------------------------------
Color
You may change colors by clicking the default colors on the right or choosing from the one above, they both work in terms of coloring the pen, shapes and text.

Eraser
The rag icon beside the default colors on the right is the eraser, it simply erases anything you've drawn, and the size can be changed as well. 

Sizes
all elements can change it's size, including pen, eraser, shapes, text etc..

Text
You may type your own text and put it into the drawboard, where you can decide the style, font, size and if it's bold or not of the text.

Shapes
3 shapes, from the left to right are rectangle, triangle and circle. The size of the shapes can be alternated as well.

undo/redo
You may use the 2 buttons with the "escape" icon on the right cornor to perform undo and redo. On the other hand, you can simply just go back a page or go front a page to preform the same action.

Clear
It erases off the entire drawboard. This action can also be undo'd by pressing the escape button or going back a page.

Save
Downloads the image on the drawboard, saves it as a png file with the name "yourdrawing.png". Should be able to find your picture in the "downloads" section.

Upload
you may choose to upload your own image into the drawboard. However, it wouldn't work if it's not an image.

Cursor
Different cursors for different functions, for example there's a shallot for the cursor when you use the eraser function, and there's a pokeball when you use the circle shape function.
it helps you find out which function your currently using easily.

Decoration
Simple and easy layout for people to understand how to use this website easily. The web title also works for many students, so thank you TA's/Professor for spending your time marking our projects. (plz gimme higher marks :()


References
drawing: https://audi.tw/Blog/JavaScript/javascript.html5.canvas.asp
         https://wcc723.github.io/canvas/2014/12/09/html5-canvas-03/
         w3school
         random fourms